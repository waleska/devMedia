from django.db import models

#É onde defino quais são os atributos que as classes vão ter (o modelo das classes).

class Pessoa(models.Model):
    nome = models.CharField(max_length=30)
    idade = models.IntegerField()
    nascimento = models.Datefield(null=True)
    email = models.EmailField(null=True)
    SEXO_CHOICES = (
        ("M", "Masculino"),
        ("F", "Feminino")
    )
    sexo = models.CharField(max_length=1, choices=SEXO_CHOICES)
    cidade = models.CharField(max_length=20)

    def __str__(self):
        return self.nome